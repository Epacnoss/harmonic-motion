use crate::{Vec3, physical_constants}; // import stuff from main.rs
use plotly::common::Mode; //import the Mode from plotly
use plotly::{Plot, Scatter}; //import Plot and Scatter from plotly

// Function for calculating Euler Method given the current time, and a set of physical constants set out in main.rs.
// Returns 3 values in a tuple - the y position, the velocity and the kinetic energy
fn euler_method(t: f32, pc: physical_constants) -> (f32, f32, f32) {
    let sqrt_km = (pc.k / pc.m).sqrt(); //get the square root of k/m - it is used often.

    //calc y
    let cos_val = (sqrt_km * t).cos();
    let y: f32 = pc.y0 * cos_val;

    //calc v
    let sin_val = (sqrt_km * t).sin();
    let before_sin = -sqrt_km * pc.y0;
    let v = before_sin * sin_val;

    //calc ke
    let ke = (pc.m * v * v) * 0.5;

    //return vals - like a python tuple
    (y, v, ke) //rust doesn't require the return keyword if you don't use a semi-colon
}

//calculate and graph the above func
pub fn graph_and_plot_euler(t_start: f32, t_end: f32, time_step: f32, pc: physical_constants) {
    let mut y_trace_vec = Vec::new(); //make arrays/lists for containing the values
    let mut v_trace_vec = Vec::new();
    let mut ke_trace_vec = Vec::new();
    let mut time_vec = Vec::new();

    let mut t: f32 = t_start; //basically - an i value, because in rust for loops cannot easily have custom increments :/

    while t <= t_end {
        let vals = euler_method(t, pc); //get the values in a tuple
        y_trace_vec.push(vals.0); //add all the values to their lists
        v_trace_vec.push(vals.1);
        ke_trace_vec.push(vals.2);
        time_vec.push(t);

        t += time_step; //the other part of the for loop
    }

    //make traces for each thing
    let y_trace = Scatter::new(time_vec.clone(), y_trace_vec) //new trace, with the time as y, and the y pos as the x
        .name("Height") //give it a nice name
        .mode(Mode::LinesMarkers); //have it plot points, and draw a line between them
    let v_trace = Scatter::new(time_vec.clone(), v_trace_vec)
        .name("Velocity")
        .mode(Mode::LinesMarkers);
    let ke_trace = Scatter::new(time_vec.clone(), ke_trace_vec)
        .name("Kinetic Energy")
        .mode(Mode::LinesMarkers);

    let mut plot = Plot::new(); //make the graph
    plot.add_trace(y_trace); //add each line
    plot.add_trace(v_trace);
    plot.add_trace(ke_trace);
    plot.to_html("shho.html"); //export to html
    // plot.show(); //open a web browser with the graph
}
