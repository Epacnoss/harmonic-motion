use crate::Vec3; //import stuff
use plotly::{Plot, Scatter};
use plotly::common::Mode;

pub const mass: f32 = 0.3; //mass of block
pub const k: f32 = 1.8; //spring constant
pub const b: f32 = 0.1; //damping constant


//get lists of values in a tuple, with the no of iterations being a given.
//TODO: work out how to get a timestep to make more precise graphs like shho.rs
fn do_stuff (iterations: usize) -> (Vec<f32>, Vec<f32>, Vec<i32>) {
    let mut vel = Vec3::new(1.0, 0.0, 0.0);
    let mut pos = Vec3::new(0.8, 0.0, 0.0); //set initial velocity and position

    let mut x_trace_vec = Vec::new(); //make lists to store values
    let mut v_trace_vec = Vec::new();
    let mut y_vec = Vec::new();

    for i in 0..iterations {
        let result = maths(&vel, &pos); //get resultant force
        vel.add_vec(result); //add force (AKA acceleration) to velocity
        pos.add_vec(vel); //add velocity to position

        x_trace_vec.push(pos.x);
        v_trace_vec.push(vel.magnitude()); //add values to graphing lists
        y_vec.push(i as i32);
    }

    (x_trace_vec, v_trace_vec, y_vec) //return graphing lists
}

pub fn plot_dampened (iter: usize) {
    let results = do_stuff(iter); //get results

    let u: Vec<f32> = results.0.iter().map(|x| {
        0.5 * k * x * x
    }).collect(); //get all of the potential energy values - map is a lambda function, and .collect makes it all into a list
    let ke: Vec<f32> = results.1.iter().map(|&v| {
        0.5 * mass * v * v //get kinetic energies
    }).collect();

    let mut k_and_u = Vec::new();
    for i in 0..ke.len() {
        k_and_u.push(u[i] + ke[i]); //get sums
    }


    let mut plot = Plot::new(); //make graph
    let x_trace = Scatter::new(results.2.clone(), results.0) //add traces for x-pos
        .name("X Position")
        .mode(Mode::LinesMarkers);
    let v_trace = Scatter::new(results.2.clone(), results.1) //for velocity magnitude
        .name("Velocity")
        .mode(Mode::LinesMarkers);

    let ke_trace = Scatter::new(results.2.clone(), ke) //for kinetic energy
        .name("Kinetic Energy")
        .mode(Mode::LinesMarkers);

    let u_trace = Scatter::new(results.2.clone(), u) //for potential energy
        .name("Potential Energy")
        .mode(Mode::LinesMarkers);
    let keu_trace = Scatter::new(results.2.clone(), k_and_u) //for sum
        .name("Kinetic Energy + Momentum")
        .mode(Mode::LinesMarkers);


    //TODO: remove tampering from stage 1 graph (maybe new func which current func uses)
    plot.add_trace(x_trace); //add exercise 1 traces to graph (I mean, it technically shouldn't have dampening, but I cba)
    plot.add_trace(v_trace);
    plot.to_html("ex1.html"); //export

    plot.add_trace(ke_trace); //add exercise 2 trace to graph
    plot.to_html("ex2.html"); //export

    plot.add_trace(u_trace); //add exercise 3 traces to graph
    plot.add_trace(keu_trace);
    plot.to_html("ex3.html"); //export

    plot.show(); //open in html
}

fn maths (vel: &Vec3, pos: &Vec3) -> Vec3 {
    let k_force = pos.mult_val_into_new(-k); //get spring force
    let dampener = vel.mult_val_into_new(-b); //get dampener

    k_force.add_vec_into_new(dampener) //add together
}