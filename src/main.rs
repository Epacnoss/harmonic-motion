mod shho; //import shho.rs
mod exercises; //import exercises.rs

use crate::shho::graph_and_plot_euler; //import the methods
use crate::exercises::plot_dampened;

#[derive(Copy, Clone, Debug)] //can be copied and printed
pub struct physical_constants { //basically like a class
    pub g: f32,
    pub m: f32,
    pub k: f32,
    pub y0: f32,
    pub v0: f32,
}

//f32 is a 32-bit float, i32 is a 32-bit integer

#[derive(Copy, Clone, Debug, Default)] //same as above; the default means that it automatically makes a constructor to set all to 0
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

//class methods
impl Vec3 {
    pub fn new (x: f32, y: f32, z: f32) -> Self { //constructor
        Vec3 {
            x, y, z
        }
    }
    fn magnitude (&self) -> f32 { //get magnitude
        //in rust, no need to import maths - all float types have all maths built in
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }
    fn mult_val (&mut self, val: f32) { //multiply self by a value - self IS changed
        self.x *= val;
        self.y *= val;
        self.z *= val;
    }
    fn mult_val_into_new (&self, val: f32) -> Self{ //multiply self by a value - self is NOT changed
        Vec3 {
            x: self.x * val,
            y: self.y * val,
            z: self.z * val
        }
    }
    fn add_vec (&mut self, o: Vec3) { //add another vector - where you see mut, self IS changed, where there is no mut, self ISN'T changed
        self.x += o.x;
        self.y += o.y;
        self.z += o.z;
    }
    fn add_vec_into_new (&self, o: Vec3) -> Self{
        Vec3 {
            x: self.x + o.x,
            y: self.y + o.y,
            z: self.z + o.z
        }
    }
    fn add_f32 (&mut self, val: f32) {
        self.x += val;
        self.y += val;
        self.z += val;
    }
    fn sub_vec (&mut self, o: Vec3) {
        self.x -= o.x;
        self.y -= o.y;
        self.z -= o.z;
    }
}

pub const SHHO_VALUES: physical_constants = physical_constants { //declaration of SHHO values - the class is just a data container
    g: 9.81,
    m: 100.0,
    k: 20.0,
    y0: 1.0,
    v0: 0.0,
};


fn main() {
    graph_and_plot_euler(0.0, 50.0, 0.25, SHHO_VALUES); //plot dem graphs
    plot_dampened(250);
}
