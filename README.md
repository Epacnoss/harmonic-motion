# Harmonic Motion

If you do not have [Cargo](https://doc.rust-lang.org/cargo/) installed, then I would reccomend just downloading the .html files - they contain the graphs. For the values - the exercise values are declared at the top of exercises.rs, and for the simple exercises the consts are declared in main.rs.

# Rust Stuff
In order to run this, you need to run 
```
cargo run
```
which could be difficult if you don't have rust installed (on Win10, that is a whole mess of its own). Most of the code is somewhat commented, and it is sort of a mix of C++, Python and Java - you have memory management, few strict type requirements, and no garbage collector.

Good luck reading it!
